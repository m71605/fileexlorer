﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public enum Units
    {
        Bytes,
        kB,
        MB,
        GB,
        TB
    }
}
